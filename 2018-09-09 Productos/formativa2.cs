using System;
using System.Collections;

class Formativa2
{
    // Creamos la clase Producto
    public class Producto{
        public int id, precio, stock, stockCritico;
        public String nombre, marca, modelo;
    }

    // Creamos la seleccion de la opcion la cual llama a despliegaMenu para crear el menu
    // y luego, mediante un do y un switch, controla el ingreso del usuario
    void seleccionaOpcion(ArrayList productos, Formativa2 formativa2)
    {
        // Por defecto asumimos que el usuario ingreso una opcion correcta
        // para evitar redundancia en los case 1, 2 y 3
        bool opcionCorrecta = true;
       
        do
        {
            // lo que nos regrese "despliegaMenu" se guarda en opcion
            int opcion = despliegaMenu(productos);

            // metemos la opcion a un switch
            switch (opcion)
            {
                case 1:
                    // Si el usuario ingresa la opcion 1 nos vamos a IngresoProducto y le enviamos el arrayList de productos
                    IngresoProducto(productos);
                    break;
                case 2:
                    // Si el usuario ingresa la opcion 2 nos vamos a ListadoProductos y le enviamos el arrayList de productos
                    ListadoProductos(productos);
                    break;
                case 3:
                    // Si el usuario ingresa la opcion 3 nos vamos a BuscarProducto y le enviamos el arrayList de productos
                    BuscarProducto(productos);
                    break;
                case 4:
                    // Si el usuario ingresa la opcion 4 cambiamos el bool a false para cerrar el ciclo while
                    opcionCorrecta = false;
                    break;
            }
        } while (opcionCorrecta);
    }

    // Creamos el despliegue del menu el cual requiere el ArrayList productos
    // para poder contar la cantidad actual de productos y mostrarlas al usuario
    // devuelve un int con la seleccion del usuario
    int despliegaMenu(ArrayList productos)
    {
        // Limpiamos la consola
        Console.Clear();

        // Dibujamos el menu
        Console.WriteLine("=== Ingrese opcion del menu ===");
        Console.WriteLine("1) Ingresar producto {0} de 10", cuentaProductos(productos)==10 ? cuentaProductos(productos) : cuentaProductos(productos)+1);
        Console.WriteLine("2) Listar productos");
        Console.WriteLine("3) Buscar producto");
        Console.WriteLine("4) Salir");
        Console.Write("> ");

        // Metemos el ingreso a un validador de int
        try
        {
            // Si se ingresa un int correctamente se devuelve el int ingresado
            int opcion = Convert.ToInt32(Console.ReadLine());
            return opcion; 
        }
        catch
        {
            // Si se ingresa un caracter no int devolvemos un mensaje y un int fuera del rango del switch de seleccionaOpcion()
            Console.WriteLine("Debe ingresar solo numeros");
            return 0;
        }

    }

    // Ingreso de productos
    void IngresoProducto(ArrayList productos)
    {
        if(cuentaProductos(productos)==10)
        {
            // Si ya se ingresaron los 10 productos no dejan pedir mas (Requerimiento de negocio)
            Console.Clear();
            Console.WriteLine("Ya se han ingresado los 10 productos");
        } else {

            // Creamos un nuevo Producto
            Producto producto = new Producto();

            DesplegarTitulo("Ingresar producto");

            // Ingreso de usuario dentro de un try-catch
            try{
                Console.Write("  Ingrese identificador (Numerico): ");
                producto.id = Convert.ToInt32(Console.ReadLine());

                // Evitamos la duplicidad de identificadores
                if(BuscarDuplicado(productos, producto.id)) throw new Exception();

                Console.Write("  Ingrese nombre: ");
                producto.nombre = Console.ReadLine();

                Console.Write("  Ingrese marca: ");
                producto.marca = Console.ReadLine();

                Console.Write("  Ingrese modelo: ");
                producto.modelo = Console.ReadLine();

                Console.Write("  Ingrese precio (Numerico): ");
                producto.precio = Convert.ToInt32(Console.ReadLine());

                Console.Write("  Ingrese stock (Numerico): ");
                producto.stock = Convert.ToInt32(Console.ReadLine());

                Console.Write("  Ingrese stock critico (Numerico): ");
                producto.stockCritico = Convert.ToInt32(Console.ReadLine());

                // Si todos los input de usuario estan correctos entonces agregamos el producto a productos
                productos.Add(producto);

            } catch {
                // Si se ingresa algo erroneo, arroja un mensaje y no realiza el .Add del producto
                Console.WriteLine("El valor ingresado no es valido");
            }

        }


        PresionarTecla();
        
    }

    // Listado de productos
    void ListadoProductos(ArrayList productos)
    {
        DesplegarTitulo("Listado de productos");

        if(productos.Count > 0)
        {
            // Se buscan todos los productos
            foreach (Producto producto in productos)
            {
                Console.WriteLine("  Identificador: {0}", producto.id);
                Console.WriteLine("  Nombre: {0}", producto.nombre);
                Console.WriteLine("  Marca: {0}", producto.marca);
                Console.WriteLine("  Modelo: {0}", producto.modelo);
                Console.WriteLine("  Precio: {0}", producto.precio);
                Console.WriteLine("  Stock: {0}", producto.stock);
                Console.WriteLine("  Stock critico: {0}", producto.stockCritico);
                Console.WriteLine("--------------------------");
            }
        } else {
            Console.WriteLine("  No se han ingresado productos");
        }
        


        PresionarTecla();
    }

    // Busqueda de productos
    void BuscarProducto(ArrayList productos)
    {
        // Bool creado para mostrar el mensaje de encontrado o no
        bool encontrado = false;

        DesplegarTitulo("Buscar producto");

        // Se valida que lo ingresado sea un identificador de tipo int
        try{
            Console.Write("Ingrese identificador a buscar: ");
            int identificador = Convert.ToInt32(Console.ReadLine());

            // Se recorren todos los productos
            foreach(Producto producto in productos)
            {
                // si el identificador ingresado corresponde al producto actual lo desplegamos
                if(identificador == producto.id)
                {
                    // Si lo encontramos, el bool encontrado pasa a ser true
                    encontrado = true;
                    Console.WriteLine("  Nombre: {0}", producto.nombre);
                    Console.WriteLine("  Marca: {0}", producto.marca);
                    Console.WriteLine("  Modelo: {0}", producto.modelo);
                    Console.WriteLine("  Precio: {0}", producto.precio);
                    Console.WriteLine("  Stock: {0}", producto.stock);
                    Console.WriteLine("  stock critico: {0}", producto.stockCritico);
                }
            }

            // Si no encontramos nada entonces le avisamos al usuario
            if(!encontrado)
                Console.WriteLine("El identificador no fue encontrado");

        } catch {
            Console.WriteLine("El valor ingresado no es valido");
        }

        PresionarTecla();
    }

    // Busqueda de identificadores duplicados
    bool BuscarDuplicado(ArrayList productos, int identificador)
    {
        bool encontrado = false;

        foreach(Producto producto in productos)
        {
            if(identificador == producto.id)
                encontrado = true;
        }

        return encontrado;     
    }

    // Presionar una tecla para continuar
    void PresionarTecla(String texto = "Presione cualquier tecla para regresar al menu")
    {
        Console.Write("\n{0}", texto);
        Console.ReadKey();
    }

    // Desplegar el titulo del menu
    void DesplegarTitulo(String texto = "")
    {
        Console.Clear();
        Console.WriteLine("=== {0} ===", texto);       
    }

    // Metodo para contar productos
    int cuentaProductos(ArrayList productos)
    {
        // regresa un Count de los productos
        return productos.Count;
    }

    // Main no puede contener mas de 20 lineas
    public static void Main()
    {
        ArrayList productos = new ArrayList();
        Formativa2 formativa2 = new Formativa2();
        formativa2.seleccionaOpcion(productos, formativa2);
    }
}
