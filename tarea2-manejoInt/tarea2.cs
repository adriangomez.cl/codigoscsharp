// Ingresar dos numeros y que aparezca en pantalla la tabla del intervalo

using System;
namespace Tarea1
{
  class Tarea1
  {
    static void Main(String[] args)
    {
      Console.Clear();

      int Inter1 = Convert.ToInt32(args[0]);
      int Inter2 = Convert.ToInt32(args[1]);

      for (int i = Inter1; i < (Inter2+1); i++)
      {
        for(int i2 = 1; i2 < 11; i2++)
        {
          Console.WriteLine("{0} x {1} = {2}", i, i2, i * i2  );
        }

        Console.WriteLine("Siguiente rango");
        Console.ReadKey();
        Console.Clear();
      }
    }
  }
}
