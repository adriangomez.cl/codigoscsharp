// Leer por teclado dos valores numericos y el operador, luego realizar la accion
// Utilizar validadores tipo try

using System;
namespace Tarea2
{
  class Tarea2
  {
    static void Main(String[] args)
    {
      String resp = "S";

      do {
        try
        {
          Console.Clear();

          Console.WriteLine("Ingrese primer numero:");
          int valor1 = Convert.ToInt32(Console.ReadLine());
          if(!(valor1 > 0 && valor1 < 1000))
            throw new Exception("Valor en un rango no valido");

          Console.WriteLine("Ingrese segundo numero:");
          int valor2 = Convert.ToInt32(Console.ReadLine());
          if(!(valor2 > 0 && valor2 < 1000))
            throw new Exception("Valor en un rango no valido");

          Console.WriteLine("Ingrese operacion a realizar:");
          String operacion = Console.ReadLine();

          switch(operacion)
          {
            default:
              Console.WriteLine("La operacion ingresada no es valida");
              break;
            case "+":
              Console.WriteLine("La suma es {0} ", valor1 + valor2);
              break;
            case "-":
              Console.WriteLine("La resta es {0} ", valor1 - valor2);
              break;
            case "*":
              Console.WriteLine("La multiplicacion es {0} ", valor1 * valor2);
              break;
            case "/":
              Console.WriteLine("La division es {0} ", valor1 / valor2);
              break;
          }
        } catch (Exception e) {
          Console.WriteLine("Excepcion -> {0} ", e.Message);

        }

        Console.WriteLine("Ingresar otro valor [S/N]?");
        resp = Console.ReadLine().Trim().ToUpper();
        if(!(resp=="S" || resp=="N"))
          throw new Exception("Valor debe ser S o N");

      } while(resp=="S");

    }
  }
}
