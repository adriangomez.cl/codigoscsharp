
using System;
namespace HolaMundo
{
  class Hello
  {
    static void Main(String[] args)
    {
      if(args.Length==2)
      {
        Console.BackgroundColor=ConsoleColor.White;
        Console.ForegroundColor=ConsoleColor.Black;
        Console.Clear();
        Console.WriteLine("Hola {0} y {1}", args[0], args[1]);

        Console.WriteLine("Presione una tecla para salir");
        Console.ReadKey();
      } else {
        Console.WriteLine("Debe pasar dos argumentos");
        Console.ReadKey();
      }

    }
  }
}
