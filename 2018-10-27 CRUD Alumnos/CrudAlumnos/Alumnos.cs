﻿using System;
using System.Data;
using System.Data.OleDb;
using ClassConexionDll;

namespace CrudAlumnos
{
    class Alumnos
    {

        OleDbDataReader datosLeidos;
        Conexion con = new Conexion("Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Taller;Data Source=.");

        int Menu()
        {
            int opcion = 0;
            try
            {
                do
                {
                    Console.Clear();
                    Console.WriteLine("=== SISTEMA DE ALUMNOS ===");
                    Console.Write(" 1) Listar todos los alumnos\n 2) Buscar por Rut \n 3) Buscar por carrera \n 4) Insertar \n 5) Borrar \n 6) Salir \n Ingrese su opcion: ");
                    opcion = Convert.ToInt32(Console.ReadLine());

                } while (opcion < 1 || opcion > 6);

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            return opcion;
        }

        int mostrarDatos(string stringSql)
        {
            int i = 0;
            if (con.abrirConexion())
            {
                datosLeidos = con.leerDatos(stringSql);
                Console.Clear();

                this.despliegaTitulo("RUT        NOMBRE                    APELLIDO PATERNO          APELLIDO MATERNO           FECH NAC  CARRERA                    FECH ING  PROMEDIO");

                while (datosLeidos.Read())
                {
                    Console.WriteLine("|{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|",
                                        datosLeidos.GetString(0),
                                        datosLeidos.GetString(1),
                                        datosLeidos.GetString(2),
                                        datosLeidos.GetString(3),
                                        datosLeidos.GetString(4),
                                        datosLeidos.GetString(5),
                                        datosLeidos.GetString(6),
                                        datosLeidos.GetDouble(7)
                                    );
                    i += 1;
                }
                con.cerrarConexion();
            } else {
                Console.WriteLine("Error: Conexion con BD, no establecida");
            }
            return i;
        }

        void delete()
        {
            string cadenaSql = "";

            this.despliegaTitulo("ELIMINACION DE ALUMNOS");

            try
            {
                Console.Write("Ingrese Rut Alumno a eliminar: ");
                String rutIngresado = Console.ReadLine();
                if (mostrarDatos("SELECT * FROM alumno WHERE rut='" + rutIngresado + "'") > 0)
                {
                    Console.Write("Esta seguro de querer eliminar el alumno con RUT {0} [S/N]", rutIngresado);

                    string resp = "";
                    do
                    {
                        resp = Console.ReadLine().ToUpper();
                    } while (!(resp == "S" || resp == "N"));

                    if (resp.Equals("S"))
                    {
                        cadenaSql = String.Format("DELETE FROM alumno WHERE rut='{0}'", rutIngresado);
                        if (con.abrirConexion())
                        {
                            if (con.ejecutarSql(cadenaSql) > 0)
                            {
                                Console.WriteLine("La operacion de elimianción se realizo correctamente");
                            } else {
                                Console.WriteLine("La operacion de eliminacion NO se realizo.");
                            }
                            con.cerrarConexion();
                        }
                    }
                } else {
                    Console.WriteLine("No se encontraron resultados");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Se ha producido una excepcion: {0}", ex.Message);
            }
        }

        void insertar()
        {
            this.despliegaTitulo("INGRESAR NUEVO ALUMNO");

            float promedioFinal;
            string cadenaSql, rut, nombre, apepat, apemat, carrera, fechanac, fechaIngreso;

            try
            {
                Console.Write("Ingrese RUT: ");
                rut = Console.ReadLine();

                Console.Write("Ingrese Nombre: ");
                nombre = Console.ReadLine();

                Console.Write("Ingrese Apellido Paterno: ");
                apepat = Console.ReadLine();

                Console.Write("Ingrese Apellido Materno : ");
                apemat = Console.ReadLine();

                reingresoFecNac:
                try
                {
                    Console.Write("Ingrese Fecha Nacimiento (DD/MM/AAAA): ");
                    fechanac = Console.ReadLine();
                    DateTime dateFechaNac = Convert.ToDateTime(fechanac);
                    fechanac = dateFechaNac.ToString("yyyy/MM/dd");
                } catch(Exception e) {
                    Console.WriteLine(" Formato de fecha no valido, vuelva a ingresar!");
                    goto reingresoFecNac;
                }


                Console.Write("Ingrese carrera: ");
                carrera = Console.ReadLine();

                reingresoFecIni:
                try
                {
                    Console.Write("Ingrese Fecha Ingreso (DD/MM/AAAAA): ");
                    fechaIngreso = Console.ReadLine();
                    DateTime dateFechaIngreso = Convert.ToDateTime(fechaIngreso);
                    fechaIngreso = dateFechaIngreso.ToString("yyyy/MM/dd");
                } catch(Exception e) {
                    Console.WriteLine(" Formato de fecha no valido, vuelva a ingresar!");
                    goto reingresoFecIni;
                }

                do
                {
                    Console.Write("Ingrese Promedio Final (1 a 7): ");
                    promedioFinal = Convert.ToSingle(Console.ReadLine());             
                } while (!(promedioFinal >= 1 && promedioFinal <= 7));

                cadenaSql = String.Format("INSERT INTO alumnos VALUES ( '{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7})",
                rut, nombre, apepat, apemat, fechanac, carrera, fechaIngreso, promedioFinal);

                if (con.abrirConexion())
                {
                    if (con.ejecutarSql(cadenaSql) > 0)
                    {
                        Console.WriteLine("Alumno ingresado correctamente");
                        
                    } else {
                        Console.WriteLine("El alumno no pudo ser ingresado");
                    }
                } else {
                    Console.WriteLine("Problema en La Conexión con BD");
                }

            } catch (Exception ex) {
                Console.WriteLine("Se ha producido una excepcion: {0}", ex.Message);
            } finally {
                con.cerrarConexion();
            }

            this.presioneTecla();
        }

        void presioneTecla()
        {
            Console.Write("\nPresione una tecla para continuar...");
            Console.ReadKey();
        }

        void despliegaTitulo(string titulo)
        {
            Console.WriteLine(titulo);

            for(int i=0; i < titulo.Length; i++)
            {
                Console.Write("=");
            }

            Console.Write("\n");
        }

        static void Main(string[] args)
        {
            Alumnos funciones = new Alumnos();

            int opcion;
            string stringSql;

            do
            {
                opcion = funciones.Menu();
                switch (Convert.ToInt32(opcion))
                {
                    case 1:
                        int num = funciones.mostrarDatos("SELECT * FROM alumnos");
                        if (num < 1)
                            Console.Write("/n --- No se encontraron alumnos en la tabla ---");

                        funciones.presioneTecla();
                        break;
                    case 2:
                        Console.Clear();
                        funciones.despliegaTitulo("BUSQUEDA DE ALUMNOS POR RUT");

                        Console.Write("\nIngrese Rut alumno: ");
                        string rutIngresado = Console.ReadLine();

                        stringSql = String.Format("SELECT * FROM alumnos WHERE rut='{0}'", rutIngresado);

                        if (funciones.mostrarDatos(stringSql) < 1)
                            Console.Write("--- No se encontro al alumno con el rut {0} ---", rutIngresado);

                        funciones.presioneTecla();

                        break;
                    case 3:
                        Console.Clear();
                        funciones.despliegaTitulo("BUSQUEDA DE ALUMNOS POR CARRERA");

                        Console.Write("\nIngrese Carrera alumno: ");
                        string carreraIngresada = Console.ReadLine();

                        stringSql = String.Format("SELECT * FROM alumnos WHERE carrera='{0}'", carreraIngresada);

                        if (funciones.mostrarDatos(stringSql) < 1)
                            Console.Write("--- No se encontraron alumnos en la carrera de {0} ---", carreraIngresada);

                        funciones.presioneTecla();

                        break;

                    case 4:
                        Console.Clear();
                        funciones.insertar();
                        break;
                    case 5:
                        Console.Clear();
                        funciones.delete();
                        break;
                    case 6:
                        Console.WriteLine("Saliendo del sistema.");
                        funciones.presioneTecla();
                        break;
                }
            } while (opcion != 6);
        }
 
    }
}
