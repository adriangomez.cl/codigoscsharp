using System;
class MetodosEstructuradosApp

{
    static void SaludoUno()
    {
        Console.WriteLine("Hola raton con cola");
    }

    static void SaludosDos(String nombre)
    {
        Console.WriteLine("Hola {0}", nombre);
    }

    static int CuadradoNumero(int numero)
    {
        return numero*numero;
    }

    static int CuboNumero(int numero)
    {
        return CuadradoNumero(numero) * numero;
    }

    static void PresioneEnterContinuar()
    {
        Console.WriteLine("Presione Enter para Continuar");
    }

    public static void Main()
    {
        String nombre = "Marcelo";
        int numero = 4;

        SaludoUno();

        PresioneEnterContinuar();

        SaludosDos(nombre);

        PresioneEnterContinuar();

        Console.WriteLine("El cuadrado de {0} es {1}", numero, CuadradoNumero(numero));

        PresioneEnterContinuar();

        Console.WriteLine("El cubo de {0} es {1}", numero, CuboNumero(numero));

        PresioneEnterContinuar();
    }
}