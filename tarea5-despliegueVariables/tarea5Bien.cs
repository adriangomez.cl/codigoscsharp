using System;
class MetodosOOApp
{
    void SaludoUno()
    {
        Console.WriteLine("Hola raton con cola");
    }

    void SaludosDos(String nombre)
    {
        Console.WriteLine("Hola {0}", nombre);
    }

    int CuadradoNumero(int numero)
    {
        return numero*numero;
    }

    int CuboNumero(int numero)
    {
        return CuadradoNumero(numero) * numero;
    }

    public void PresioneEnterContinuar()
    {
        Console.WriteLine("Presione Enter para Continuar");
        Console.ReadKey();
    }

    public static void Main()
    {
        String nombre = "Marcelo";
        int numero = 4;

        MetodosOOApp objeto = new MetodosOOApp();

        objeto.SaludoUno();

        objeto.PresioneEnterContinuar();

        objeto.SaludosDos(nombre);

        objeto.PresioneEnterContinuar();

        Console.WriteLine("El cuadrado de {0} es {1}", numero, objeto.CuadradoNumero(numero));

        objeto.PresioneEnterContinuar();

        Console.WriteLine("El cubo de {0} es {1}", numero, objeto.CuboNumero(numero));

        objeto.PresioneEnterContinuar();
    }
}