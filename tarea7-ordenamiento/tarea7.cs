using System;
using System.Collections.Generic;
using System.Linq;

class linqNumero
{
    public static void Main(String[] a)
    {
        int[] listaNumeros = {2,3,4,5,6};

        IEnumerable<int> listaResultado = from num in listaNumeros orderby num descending select num;
        
        foreach(int numero in listaResultado)
            Console.WriteLine(numero);

        Console.WriteLine("************* Numeros pares *************");
        listaResultado = from num in listaNumeros where num % 2 == 0 select num;
        foreach(int numero in listaResultado)
            Console.WriteLine(numero);

        Console.WriteLine("************* Numero maximo *************");
        listaResultado=from num in listaNumeros select num;
        int maximo = listaResultado.Max();
        Console.WriteLine(maximo);

        Console.WriteLine("************* Numero Minimo *************");
        listaResultado=from num in listaNumeros select num;
        int minimo = listaResultado.Min();
        Console.WriteLine(minimo);

        Console.WriteLine("************* Promedio *************");
        listaResultado=from num in listaNumeros select num;
        double promedio=listaResultado.Average();
        Console.WriteLine(promedio);

    }
}