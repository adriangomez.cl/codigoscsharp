using System;

class X{
    int a = 2;
    void Saluda()
    {
        Console.WriteLine("Hola");
    }

    public static void Main()
    {
        X objeto = new X();
        objeto.a = objeto.a+2;
        objeto.Saluda();
        Console.WriteLine("Valor de a:{0}.", objeto.a);
    }
}