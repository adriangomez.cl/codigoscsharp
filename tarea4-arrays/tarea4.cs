// Ingresa dos alumnos y los despliega

using System;

class programaStruct
{
  struct alumno
  {
    public String rut;
    public String nombre;
    public String apellidos;
    public double promedio;
  }

  static int cantidad = 2;

  public static void Main()
  {
    Console.Clear();

    alumno[] listaAlumnos = new alumno[cantidad];

    for(int i=0; i<cantidad; i++)
    {
      Console.Write("Ingrese rut:");
      listaAlumnos[i].rut = Console.ReadLine();

      Console.Write("Ingrese nombre:");
      listaAlumnos[i].nombre = Console.ReadLine();

      Console.Write("Ingrese apellidos:");
      listaAlumnos[i].apellidos = Console.ReadLine();

      Console.Write("Ingrese promedio:");
      listaAlumnos[i].promedio = Convert.ToDouble(Console.ReadLine());

      Console.Clear();

    }

    for(int i=0; i<cantidad; i++)
      Console.WriteLine("{0}\t{1}\t{2}\t{3}", listaAlumnos[i].rut, listaAlumnos[i].nombre, listaAlumnos[i].apellidos, listaAlumnos[i].promedio);

  }
}
